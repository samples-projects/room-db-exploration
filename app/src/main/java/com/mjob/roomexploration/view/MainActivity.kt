package com.mjob.roomexploration.view

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.mjob.roomexploration.R
import com.mjob.roomexploration.di.factory.ViewModelFactory
import com.mjob.roomexploration.model.entity.Book
import com.mjob.roomexploration.viewmodel.BookViewModel
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : DaggerAppCompatActivity(), BookActionListener {
    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    lateinit var bookViewModel: BookViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        bookViewModel = ViewModelProviders.of(this, viewModelFactory).get(BookViewModel::class.java)
        bookViewModel.booksLiveData.observe(this,
            Observer<List<Book>> { books ->
                initBookList(books)
            })

        fab.setOnClickListener {
            val intent = Intent(this, NewBookActivity::class.java)
            startActivity(intent)
        }
    }

    override fun deleteBook(book: Book) {
        bookViewModel.delete(book)
    }

    override fun editBook(book: Book) {
        val intent = Intent(this, EditBookActivity::class.java)
        intent.putExtra("book", book)
        startActivity(intent)
    }

    private fun initBookList(books: List<Book>) {
        books_list.adapter = BookListAdapter(books, this)
        books_list.layoutManager = LinearLayoutManager(this)
    }
}

interface BookActionListener {
    fun deleteBook(book: Book)
    fun editBook(book: Book)
}
