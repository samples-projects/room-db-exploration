package com.mjob.roomexploration.view

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.mjob.roomexploration.R
import com.mjob.roomexploration.di.factory.ViewModelFactory
import com.mjob.roomexploration.model.entity.Book
import com.mjob.roomexploration.viewmodel.BookViewModel
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_new_book.*
import javax.inject.Inject

class NewBookActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var bookViewModel: BookViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_book)

        bookViewModel = ViewModelProviders.of(this, viewModelFactory).get(BookViewModel::class.java)

        create_book_btn.setOnClickListener { view ->
            if (bookAuthor.text.isBlank() || bookTitle.text.isBlank()) {
                showMessage(container, getString(R.string.error_create_book))
            } else {
                val book = Book(author = bookAuthor.text.toString(), title = bookTitle.text.toString())
                bookViewModel.insert(book)
                showMessage(view, getString(R.string.success_create_book), ::redirectToMainActivity)
            }
        }
    }

    private fun redirectToMainActivity() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

    private fun showMessage(view: View, message: String, onDismissedSnackBarCallback: (() -> Unit)? = null) {
        val snackBar = Snackbar.make(view, message, Snackbar.LENGTH_LONG)
        onDismissedSnackBarCallback?.let {
            snackBar.addCallback(object : BaseTransientBottomBar.BaseCallback<Snackbar>() {
                override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                    onDismissedSnackBarCallback.invoke()
                }
            })
        }
        snackBar.show()
    }
}
