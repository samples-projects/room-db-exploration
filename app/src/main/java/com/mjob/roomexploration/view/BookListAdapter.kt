package com.mjob.roomexploration.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.mjob.roomexploration.R
import com.mjob.roomexploration.model.entity.Book

class BookListAdapter(private val books: List<Book>, private val bookActionListener: BookActionListener) :
    RecyclerView.Adapter<BookViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_book, parent, false)
        return BookViewHolder(view)
    }

    override fun getItemCount(): Int = books.size

    override fun onBindViewHolder(holder: BookViewHolder, position: Int) {
        holder.bindBookToItemView(books[position], bookActionListener)
    }
}

class BookViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    private lateinit var title: TextView
    private lateinit var author: TextView
    private lateinit var editBookIcon: ImageView
    private lateinit var deleteBookIcon: ImageView
    fun bindBookToItemView(
        book: Book,
        bookActionListener: BookActionListener
    ) {
        title = itemView.findViewById(R.id.book_title)
        title.text = book.title

        author = itemView.findViewById(R.id.book_author)
        author.text = book.author

        editBookIcon = itemView.findViewById(R.id.book_edit)
        editBookIcon.setOnClickListener {
            bookActionListener.editBook(book)
        }

        deleteBookIcon = itemView.findViewById(R.id.book_delete)
        deleteBookIcon.setOnClickListener {
            bookActionListener.deleteBook(book)
        }
    }
}