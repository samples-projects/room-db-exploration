package com.mjob.roomexploration

import com.mjob.roomexploration.di.DaggerRoomApplicationComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

class RoomApplication : DaggerApplication() {
    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerRoomApplicationComponent.builder()
            .create(this)
    }
}