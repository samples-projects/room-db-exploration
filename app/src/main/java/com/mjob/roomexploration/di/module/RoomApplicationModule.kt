package com.mjob.roomexploration.di.module

import android.content.Context
import androidx.room.Room
import com.mjob.roomexploration.RoomApplication
import com.mjob.roomexploration.model.dao.BookDao
import com.mjob.roomexploration.model.database.BookRoomDatabase
import com.mjob.roomexploration.model.repository.BookRepository
import com.mjob.roomexploration.model.repository.impl.LocalBookRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RoomApplicationModule {

    @Singleton
    @Provides
    fun provideContext(application: RoomApplication): Context = application

    @Singleton
    @Provides
    fun provideRoomDatabase(context: Context): BookRoomDatabase {
        val databaseName = "book_database"
        var bookRoomInstance: BookRoomDatabase? = null

        if (bookRoomInstance == null) {
            synchronized(BookRoomDatabase::class.java) {
                bookRoomInstance = buildDatabase(context, databaseName)
            }
        }
        return bookRoomInstance!!
    }

    @Singleton
    @Provides
    fun provideRoomDao(database: BookRoomDatabase): BookDao = database.bookDao()

    @Singleton
    @Provides
    fun provideBookRepository(dao: BookDao): BookRepository = LocalBookRepository(dao)

    private fun buildDatabase(context: Context, databaseName: String): BookRoomDatabase {
        return Room.databaseBuilder<BookRoomDatabase>(
            context.applicationContext,
            BookRoomDatabase::class.java,
            databaseName
        ).build()
    }
}