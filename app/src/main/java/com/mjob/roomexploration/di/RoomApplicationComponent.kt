package com.mjob.roomexploration.di

import com.mjob.roomexploration.RoomApplication
import com.mjob.roomexploration.di.module.ActivityModule
import com.mjob.roomexploration.di.module.RoomApplicationModule
import com.mjob.roomexploration.di.module.ViewModelModule
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ViewModelModule::class,
        ActivityModule::class,
        RoomApplicationModule::class]
)
interface RoomApplicationComponent : AndroidInjector<RoomApplication> {
    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<RoomApplication>()
}