package com.mjob.roomexploration.di.module

import com.mjob.roomexploration.view.EditBookActivity
import com.mjob.roomexploration.view.MainActivity
import com.mjob.roomexploration.view.NewBookActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {
    @ContributesAndroidInjector
    abstract fun contributesMainActivity(): MainActivity

    @ContributesAndroidInjector
    abstract fun contributesNewBookActivity(): NewBookActivity

    @ContributesAndroidInjector
    abstract fun contributesEditBookActivity(): EditBookActivity
}