package com.mjob.roomexploration.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.mjob.roomexploration.di.ViewModelKey
import com.mjob.roomexploration.di.factory.ViewModelFactory
import com.mjob.roomexploration.viewmodel.BookViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    /*
     * This method basically says
     * inject this object into a Map using the @IntoMap annotation,
     * with the  MovieListViewModel.class as key,
     * and a Provider that will build a MovieListViewModel
     * object.
     *
     * */

    @Binds
    @IntoMap
    @ViewModelKey(BookViewModel::class)
    internal abstract fun provideBookViewModel(bookViewModel: BookViewModel): ViewModel
}