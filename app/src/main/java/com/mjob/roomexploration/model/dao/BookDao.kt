package com.mjob.roomexploration.model.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Delete
import androidx.room.Update
import com.mjob.roomexploration.model.entity.Book

@Dao
interface BookDao {
    @Insert
    fun insert(book: Book)

    @Query("SELECT * FROM books")
    fun getAllBooks(): LiveData<List<Book>>

    @Delete
    fun delete(book: Book)

    @Update
    fun update(book: Book)
}