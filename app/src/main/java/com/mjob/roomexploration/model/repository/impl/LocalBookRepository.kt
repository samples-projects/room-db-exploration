package com.mjob.roomexploration.model.repository.impl

import androidx.lifecycle.LiveData
import com.mjob.roomexploration.model.dao.BookDao
import com.mjob.roomexploration.model.entity.Book
import com.mjob.roomexploration.model.repository.BookRepository
import javax.inject.Inject

class LocalBookRepository @Inject constructor(private val dao: BookDao) : BookRepository {
    override fun createBook(book: Book) {
        dao.insert(book)
    }

    override fun deleteBook(book: Book) {
        dao.delete(book)
    }

    override fun updateBook(book: Book) {
        dao.update(book)
    }

    override fun getBooks(): LiveData<List<Book>> {
        return dao.getAllBooks()
    }
}