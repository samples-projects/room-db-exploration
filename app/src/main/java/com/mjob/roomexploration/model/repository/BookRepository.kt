package com.mjob.roomexploration.model.repository

import androidx.lifecycle.LiveData
import com.mjob.roomexploration.model.entity.Book

interface BookRepository {
    fun createBook(book: Book)
    fun deleteBook(book: Book)
    fun updateBook(book: Book)
    fun getBooks(): LiveData<List<Book>>
}