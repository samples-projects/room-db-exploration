package com.mjob.roomexploration.model.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.mjob.roomexploration.model.dao.BookDao
import com.mjob.roomexploration.model.entity.Book

@Database(entities = [Book::class], version = 1)
abstract class BookRoomDatabase : RoomDatabase() {
    abstract fun bookDao(): BookDao
}