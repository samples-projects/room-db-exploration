package com.mjob.roomexploration.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.mjob.roomexploration.model.entity.Book
import com.mjob.roomexploration.model.repository.BookRepository
import javax.inject.Inject
import kotlin.concurrent.thread

class BookViewModel @Inject constructor(private val bookRepository: BookRepository) : ViewModel() {
    val booksLiveData: LiveData<List<Book>> = bookRepository.getBooks()

    fun insert(book: Book) {
        thread {
            bookRepository.createBook(book)
        }
    }

    fun delete(book: Book) {
        thread {
            bookRepository.deleteBook(book)
        }
    }

    fun update(book: Book) {
        thread {
            bookRepository.updateBook(book)
        }
    }
}