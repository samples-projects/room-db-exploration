# Room-db-exploration

Sample project of listing book to read in future. using Room local Database

# Technology
This project is a picture gallery built with :
- [Room](https://developer.android.com/training/data-storage/room/index.html)
- [MVVM](https://developer.android.com/jetpack/docs/guide)  

Some other tools are used for project quality and commodity : 
- [Dagger2](https://github.com/google/dagger) for injecting dependencies
- [Ktlint](https://github.com/pinterest/ktlint) for code formatting following rules
- [Detekt](https://github.com/arturbosch/detekt) for static code analysis

# Architecture

```mermaid
graph LR
A((Activity)) --> B((ViewModel))
B --> A
B --> C((Repository))
C --> D(Room)
```
